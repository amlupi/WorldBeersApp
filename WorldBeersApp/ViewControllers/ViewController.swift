//
//  ViewController.swift
//  WorldBeersApp
//
//  Created by Andrea Maria Lupi on 09/03/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView?

    
 
    let searchController = UISearchController(searchResultsController: nil)
    var filteredBeer: [Beer] = []
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search for beers"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.register(BeerTableViewCell.nib(), forCellReuseIdentifier: "BeerTableViewCell")
    
        // Do any additional setup after loading the view.
        getBeersFromApi()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToDetail"){
            let detailViewController = segue.destination as? DetailViewController
           
            let indexPath = self.tableView?.indexPathForSelectedRow
            if isFiltering {
                let concatenatedFoodPairing = filteredBeer[indexPath!.row].food_pairing.joined(separator: ", ")
                detailViewController?.navigationItem.title = filteredBeer[indexPath!.row].name
                detailViewController!.firstBrewed = filteredBeer[indexPath!.row].first_brewed
                detailViewController!.foodPairing = concatenatedFoodPairing
                detailViewController!.brewersTips = filteredBeer[indexPath!.row].brewers_tips
            } else {
                let concatenatedFoodPairing = beerList[indexPath!.row].food_pairing.joined(separator: ", ")
                detailViewController?.navigationItem.title = beerList[indexPath!.row].name
                detailViewController!.firstBrewed = beerList[indexPath!.row].first_brewed
                detailViewController!.foodPairing = concatenatedFoodPairing
                detailViewController!.brewersTips = beerList[indexPath!.row].brewers_tips
            }
          
            
        }
    }
    
    func getBeersFromApi() {
        let url = URL(string: "https://api.punkapi.com/v2/beers")!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                let beerListResponse = try? JSONDecoder().decode([Beer].self, from: data)
                beerList = beerListResponse!
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
        }
        task.resume()
        
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filteredBeer.count
        }
        return beerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BeerTableViewCell.cellIdentifier) as! BeerTableViewCell
        let model: Beer!
        if isFiltering {
            model = filteredBeer[indexPath.row]
        } else {
            model = beerList[indexPath.row]
        }
        
        cell.configure(with: CellViewModel(image: model.image_url, name: model.name, description: model.description, abv: model.abv, ibu: model.ibu))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "goToDetail", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
        
    }
 
    func filterContentForSearchText(_ searchText: String) {
        filteredBeer = beerList.filter { (beer: Beer) -> Bool in
            return beer.name.lowercased().contains(searchText.lowercased()) || beer.description.lowercased().contains(searchText.lowercased())
        }
        tableView?.reloadData()
    }
}

extension ViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}



