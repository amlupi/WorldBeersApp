//
//  DetailViewController.swift
//  WorldBeersApp
//
//  Created by Andrea Maria Lupi on 09/03/22.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var firstBrewedLabel: UILabel!
    @IBOutlet weak var foodPairingLabel: UILabel!
    @IBOutlet weak var brewersTipsLabel: UILabel!
    
    var firstBrewed: String!
    var foodPairing: String!
    var brewersTips: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstBrewedLabel.text = firstBrewed
        foodPairingLabel.text = foodPairing
        brewersTipsLabel.text = brewersTips

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
