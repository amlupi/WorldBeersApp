//
//  BeerModel.swift
//  WorldBeersApp
//
//  Created by Andrea Maria Lupi on 09/03/22.
//

import UIKit

var beerList = [Beer]()

struct Beer: Codable{
    
    let id: Int
    let image_url: URL
    let name: String
    let description: String
    let abv: Float?
    let ibu: Float?
    let first_brewed: String
    let brewers_tips: String
    let food_pairing: [String]

}
