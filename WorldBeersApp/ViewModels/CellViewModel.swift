//
//  CellViewModel.swift
//  WorldBeersApp
//
//  Created by Andrea Maria Lupi on 09/03/22.
//

import UIKit

struct CellViewModel {
    let image: URL
    let name: String
    let description: String
    var abv: Float?
    var ibu: Float?
}
