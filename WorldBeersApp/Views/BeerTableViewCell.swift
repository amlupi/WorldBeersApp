//
//  BeerTableViewCell.swift
//  WorldBeersApp
//
//  Created by Andrea Maria Lupi on 09/03/22.
//

import UIKit
import Kingfisher

class BeerTableViewCell: UITableViewCell {
    

    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ibuLabel: UILabel!
    @IBOutlet weak var abvLabel: UILabel!
    
    
    
    static let cellIdentifier = "BeerTableViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }


  
    
    static func nib() -> UINib {
        return UINib(nibName: "BeerTableViewCell", bundle: nil)
    }
    
    public func configure(with viewModel: CellViewModel) {
        nameLabel.text = viewModel.name
        descriptionLabel.text = viewModel.description
        descriptionLabel.sizeToFit()
//        let imageData = try! Data(contentsOf: viewModel.image)
        let resource = ImageResource(downloadURL: viewModel.image)
//        let processor = RoundCornerImageProcessor(cornerRadius: 20)
        self.beerImage.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        DispatchQueue.global().async {
         
                DispatchQueue.main.async {
                  
            
            }

        }
                    
//        let libraryDirectory = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
//        let libraryURL = URL(fileURLWithPath: libraryDirectory, isDirectory: true)
//        let data = UIImageJPEGRepresentation(viewModel.image, 1.0)
//        do {
//            let imageData = try NSData(contentsOf: viewModel.image, options: NSData.ReadingOptions())
//            beerImage.image = UIImage(data: imageData as Data, scale: 0.5)
//        } catch {
//            print(error)
//        }
 
        
      
        
        if viewModel.abv == nil {
            abvLabel.text = "ABV: N/A"
        } else {
            abvLabel.text = "ABV: \(String(viewModel.abv!))%"
        }
        if viewModel.ibu == nil {
            abvLabel.text = "IBU: N/A"
        } else{
            ibuLabel.text = "IBU: \(String(viewModel.ibu!))"
        }
        
        
    }
}
